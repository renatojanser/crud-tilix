var app = require('./config/server');
var connection = require('./config/connection');

var rotaHome = require('./app/routes/home')(app, connection);
var retornaFaturas = require('./app/routes/faturas')(app, connection);
var formFatura = require('./app/routes/formFatura')(app, connection);
var excluirFatura = require('./app/routes/excluirFatura')(app, connection);

app.listen(3000, function(){
    console.log("Servidor ON");
});