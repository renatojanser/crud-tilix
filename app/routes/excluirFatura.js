module.exports = function(app, connection){
    app.get('/excluirFaturas/:id', function(req,res){

        const id = parseInt(req.params.id);
        
        connection.query('DELETE FROM fatura WHERE idfatura='+id, function(error, results, fields){
            if(error) 
                res.json(error);
            else
                res.redirect('/');
        });
    });
}