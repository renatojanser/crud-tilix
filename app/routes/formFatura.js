module.exports = function(app, connection){
    

    app.get('/fatura', function(req,res){
        res.render('formFatura');
    });
    app.post('/fatura', (req, res) =>{
        const nome = req.body.nome;
        const valor = req.body.valor;
        const data_vencimento = req.body.data_vencimento;
        const pagou = req.body.pagou;
        const usuario = req.body.usuario;
        
            
        connection.query(`INSERT INTO fatura(idusuario, nome_empresa, valor, data_vencimento, pagou) VALUES('${usuario}', '${nome}' , '${valor}', '${data_vencimento}', '${pagou}')`, function(error, results, fields){
            if(error) 
                res.json(error);
            else
                res.redirect('/');
                // res.json(results);
        });
    });
    app.get('/pegarFatura/:id', (req, res) =>{
        const id = parseInt(req.params.id);

        connection.query('SELECT * FROM fatura WHERE idfatura = '+id, function(error, results, fields){
            if(error) 
                res.json(error);
            else
                res.json(results);
        });
    });
    app.get('/fatura/:id', (req, res) =>{
        res.render('editarFatura');
    });
    app.post('/fatura/:id', (req, res) =>{
        const id = parseInt(req.params.id);
        const nome = req.body.nome;
        const valor = req.body.valor;
        const data_vencimento = req.body.data_vencimento;
        const pagou = req.body.pagou;
        const usuario = req.body.usuario;

        connection.query(`UPDATE fatura SET idusuario='${usuario}', nome_empresa='${nome}', valor='${valor}', data_vencimento='${data_vencimento}', pagou='${pagou}' WHERE idfatura=${id}`, function(error, results, fields){
            if(error) 
                res.json(error);
            else
                res.redirect('/');
                // res.json(results);
        });
    });
}