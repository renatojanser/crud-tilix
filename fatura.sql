/*
SQLyog Community- MySQL GUI v8.22 
MySQL - 5.7.26 : Database - tilix
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tilix` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tilix`;

/*Table structure for table `fatura` */

DROP TABLE IF EXISTS `fatura`;

CREATE TABLE `fatura` (
  `idfatura` int(11) NOT NULL AUTO_INCREMENT,
  `idusuario` int(11) DEFAULT NULL,
  `nome_empresa` varchar(255) DEFAULT NULL,
  `valor` decimal(10,0) DEFAULT NULL,
  `data_vencimento` date DEFAULT NULL,
  `pagou` enum('Sim','Não') DEFAULT 'Não',
  PRIMARY KEY (`idfatura`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `fatura` */

insert  into `fatura`(`idfatura`,`idusuario`,`nome_empresa`,`valor`,`data_vencimento`,`pagou`) values (2,1,'teste','10','2019-10-10','Sim');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
